import app.Application;
import app.MainDataStore;
import controllers.MainController;
import presentation.EnterForm;

public class Main {

    public static void main(String[] args) {
        MainDataStore.createStore();

        EnterForm enterForm = new EnterForm();
        enterForm.setVisible(true);
    }
}
