package app;

public class Application {

    private static Application application;

    private ApplicationComponent component;

    public static Application getInstance() {
        if (application == null) {
            application = new Application();
        }

        return application;
    }

    private Application() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
