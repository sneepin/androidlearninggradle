package app;

import controllers.MainController;
import dagger.Component;
import dao.AssemblyOrFinishedPartDao;

import javax.inject.Singleton;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    MainController getMainController();
}
