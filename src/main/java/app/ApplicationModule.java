package app;

import controllers.MainController;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class ApplicationModule {

    @Singleton
    @Provides
    MainController provideMainController() {
        return new MainController();
    }
}
