package app;

import entity.AssemblyOrFinishedPart;
import entity.Profession;
import entity.Sign;
import entity.SpentTimes;
import utils.*;

import java.util.ArrayList;
import java.util.List;


public class MainDataStore {

    public static final List<DatabaseList> mainDataList = new ArrayList<>();
    public static DatabaseList<AssemblyOrFinishedPart> parts;
    public static DatabaseList<Sign> signs;
    public static DatabaseList<SpentTimes> spentTimes;
    public static DatabaseList<Profession> professions;

    public static void createStore() {
        parts = new DatabaseList<>(AssemblyOrFinishedPart.class);
//        parts.add(new AssemblyOrFinishedPart("sadasd"));
//        parts.add(new AssemblyOrFinishedPart("sadasd"));
//        parts.add(new AssemblyOrFinishedPart("sadasd"));
        mainDataList.add(parts);

        signs = new DatabaseList<>(Sign.class);
        signs.add(new Sign("сборочная единица"));
        signs.add(new Sign("готовая деталь"));
        mainDataList.add(signs);

        spentTimes = new DatabaseList<>(SpentTimes.class);
//        spentTimes.add(new SpentTimes());
//        spentTimes.add(new SpentTimes());
//        spentTimes.add(new SpentTimes());
        mainDataList.add(spentTimes);

        professions = new DatabaseList<>(Profession.class);
//        professions.add(new Profession("sadas"));
//        professions.add(new Profession("sadas"));
//        professions.add(new Profession("sadas"));
        mainDataList.add(professions);
    }
}
