package controllers;


import app.MainDataStore;
import entity.AssemblyOrFinishedPart;
import entity.Sign;
import presentation.TableFrame;
import table_models.BaseTableModel;
import table_models.Column;

import java.util.ArrayList;
import java.util.List;


public class MainController {

    public void showAllAssembliesOrFinishedParts() {
        List<AssemblyOrFinishedPart> dataList = MainDataStore.parts;

        List<Column> columns = new ArrayList<>();
        columns.add(new Column("Наименование", "nomination", Column.EDIT_TEXT));
        columns.add(new Column("Тип", "nameSign", Column.SPINNER, Sign.class));
//        columns.add(new Column("Номер госта", "gostNumber"));

        BaseTableModel<AssemblyOrFinishedPart> tableModel = new BaseTableModel<>(dataList, columns);

        TableFrame<AssemblyOrFinishedPart> frame = new TableFrame<>(tableModel);

        frame.setAddButtonClickDelegate(() -> {
            dataList.add(new AssemblyOrFinishedPart("asd"));

            return dataList.size() - 1;
        });
        frame.setRemoveItemDelegate(dataList::remove);
        frame.setRemoveLastAddedElementDelegate(() -> dataList.remove(dataList.size() - 1));

        frame.setSaveButtonClickListener((map, objectMap, item) -> {
//            item.setId(Integer.parseInt(map.get("Код детали")));
//            item.setGostNumber(map.get("Номер госта"));
            item.setNomination(map.get("Наименование"));

            item.setSign((Sign) objectMap.get("Тип"));
//            item.setGostDate(map.get("Дата госта"));

            tableModel.fireTableDataChanged();
        });

        frame.setVisible(true);
    }

    public void showProfessions() {
        List<AssemblyOrFinishedPart> dataList = MainDataStore.parts;

        List<Column> columns = new ArrayList<>();
        columns.add(new Column("Наименование", "nomination", Column.EDIT_TEXT));
        columns.add(new Column("Тип", "nameSign", Column.SPINNER, Sign.class));
//        columns.add(new Column("Номер госта", "gostNumber"));

        BaseTableModel<AssemblyOrFinishedPart> tableModel = new BaseTableModel<>(dataList, columns);

        TableFrame<AssemblyOrFinishedPart> frame = new TableFrame<>(tableModel);

        frame.setAddButtonClickDelegate(() -> {
            dataList.add(new AssemblyOrFinishedPart("asd"));

            return dataList.size() - 1;
        });
        frame.setRemoveItemDelegate(dataList::remove);
        frame.setRemoveLastAddedElementDelegate(() -> dataList.remove(dataList.size() - 1));

        frame.setSaveButtonClickListener((map, objectMap, item) -> {
//            item.setId(Integer.parseInt(map.get("Код детали")));
//            item.setGostNumber(map.get("Номер госта"));
            item.setNomination(map.get("Наименование"));

            item.setSign((Sign) objectMap.get("Тип"));
//            item.setGostDate(map.get("Дата госта"));

            tableModel.fireTableDataChanged();
        });

        frame.setVisible(true);
    }

    public void showSpentTimes() {
        List<AssemblyOrFinishedPart> dataList = MainDataStore.parts;

        List<Column> columns = new ArrayList<>();
        columns.add(new Column("Наименование", "nomination", Column.EDIT_TEXT));
        columns.add(new Column("Тип", "nameSign", Column.SPINNER, Sign.class));
//        columns.add(new Column("Номер госта", "gostNumber"));

        BaseTableModel<AssemblyOrFinishedPart> tableModel = new BaseTableModel<>(dataList, columns);

        TableFrame<AssemblyOrFinishedPart> frame = new TableFrame<>(tableModel);

        frame.setAddButtonClickDelegate(() -> {
            dataList.add(new AssemblyOrFinishedPart("asd"));

            return dataList.size() - 1;
        });
        frame.setRemoveItemDelegate(dataList::remove);
        frame.setRemoveLastAddedElementDelegate(() -> dataList.remove(dataList.size() - 1));

        frame.setSaveButtonClickListener((map, objectMap, item) -> {
//            item.setId(Integer.parseInt(map.get("Код детали")));
//            item.setGostNumber(map.get("Номер госта"));
            item.setNomination(map.get("Наименование"));

            item.setSign((Sign) objectMap.get("Тип"));
//            item.setGostDate(map.get("Дата госта"));

            tableModel.fireTableDataChanged();
        });

        frame.setVisible(true);
    }
}
