package dao;


import app.ConnectionCreater;
import entity.AssemblyOrFinishedPart;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssemblyOrFinishedPartDao {

    private ConnectionCreater connectionCreater;

    public AssemblyOrFinishedPartDao(ConnectionCreater connectionCreater) {
        this.connectionCreater = connectionCreater;
    }

    public List<AssemblyOrFinishedPart> getAllAssembliesOrFinishedParts() throws SQLException, ClassNotFoundException {
        Connection connection = connectionCreater.createConnection();

        String query = "Select * From directoryofassembliesandfinishedparts;";

        List<AssemblyOrFinishedPart> resultList = new ArrayList<>();

        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next()) {
            int code = resultSet.getInt(1);

            AssemblyOrFinishedPart part = new AssemblyOrFinishedPart();
            part.setId(code);

            resultList.add(part);
        }

        return resultList;
    }
}

