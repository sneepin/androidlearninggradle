package entity;


import java.util.List;

public class AssemblyOrFinishedPart {

    private int id;

    private Sign sign;

    private String gostNumber;

    private String gostDate;

    private String nomination;

    public AssemblyOrFinishedPart() {
        sign = new Sign("gus suka");
    }

    public AssemblyOrFinishedPart(String nomination) {
        this.nomination = nomination;

        sign = new Sign("gus suka");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sign getSign() {
        return sign;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }

    public String getGostNumber() {
        return gostNumber;
    }

    public void setGostNumber(String gostNumber) {
        this.gostNumber = gostNumber;
    }

    public String getGostDate() {
        return gostDate;
    }

    public void setGostDate(String gostDate) {
        this.gostDate = gostDate;
    }

    public String getNomination() {
        return nomination;
    }

    public void setNomination(String nomination) {
        this.nomination = nomination;
    }
}
