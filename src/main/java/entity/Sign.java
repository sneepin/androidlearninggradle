package entity;


public class Sign {

    private String nameSign;

    public Sign(String nameSign) {
        this.nameSign = nameSign;
    }

    public String getNameSign() {
        return nameSign;
    }

    public void setNameSign(String nameSign) {
        this.nameSign = nameSign;
    }

    @Override
    public String toString() {
        return nameSign;
    }
}
