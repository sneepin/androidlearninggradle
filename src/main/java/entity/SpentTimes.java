package entity;

public class SpentTimes {

    private AssemblyOrFinishedPart part;

    private Profession profession;

    private int spentBeforeTime;

    private int spentForOperationTime;

    public AssemblyOrFinishedPart getPart() {
        return part;
    }

    public void setPart(AssemblyOrFinishedPart part) {
        this.part = part;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public int getSpentBeforeTime() {
        return spentBeforeTime;
    }

    public void setSpentBeforeTime(int spentBeforeTime) {
        this.spentBeforeTime = spentBeforeTime;
    }

    public int getSpentForOperationTime() {
        return spentForOperationTime;
    }

    public void setSpentForOperationTime(int spentForOperationTime) {
        this.spentForOperationTime = spentForOperationTime;
    }
}
