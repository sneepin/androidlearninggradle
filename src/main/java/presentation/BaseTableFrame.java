package presentation;

import table_models.BaseTableModel;

import javax.swing.*;
import java.awt.*;


public abstract class BaseTableFrame extends JFrame {

    public BaseTableFrame() {
        initComponents();

        jTable1.setBackground(new Color(110, 193, 248));
        jTable1.setForeground(Color.white);
        jTable1.setRowHeight(30);
        jTable1.setFont(new Font("Serif", Font.BOLD, 20));

        jTable1.getTableHeader().setOpaque(false);
        jTable1.getTableHeader().setFont(new Font("Serif", Font.BOLD, 24));

        saveButton.setVisible(false);
        cancelButton.setVisible(false);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    protected boolean showErorDialog() {
        if (jTable1.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Ничего не выбрано");
            return true;
        }

        return false;
    }

    protected void showEditPanel() {
        saveButton.setVisible(true);
        cancelButton.setVisible(true);

        editButton.setEnabled(false);
        addButton.setEnabled(false);
        deleteButton.setEnabled(false);
    }

    protected void showTable() {
        saveButton.setVisible(false);
        cancelButton.setVisible(false);

        jScrollPane1.setViewportView(jTable1);

        editButton.setEnabled(true);
        addButton.setEnabled(true);
        deleteButton.setEnabled(true);
    }

    protected abstract void onAddClick();

    protected abstract void onEdit();

    protected abstract void onDelete();

    protected abstract void onSave();

    protected abstract void onCancel();

    private void initComponents() {
        jPanel1 = new JPanel();
        jPanel2 = new JPanel();

        jScrollPane1 = new JScrollPane();

        jTable1 = new JTable();

        addButton = new JButton();
        editButton = new JButton();
        deleteButton = new JButton();
        saveButton = new JButton();
        cancelButton = new JButton();

        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1.setBackground(new java.awt.Color(47, 86, 233));
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );
//
//        setDefaultCloseOperation(WindowConstants.);

        jScrollPane1.setViewportView(jTable1);

        addButton.setText("Добавить");
        addButton.addActionListener(evt -> {
            onAddClick();

//            state = State.ADD;
//
//            addButtonClickListener.onClick();
        });

        editButton.setText("Править");
        editButton.addActionListener(evt -> {
            onEdit();

//            state = State.EDIT;
//
//            if (jTable1.getSelectedRow() == -1) {
//                JOptionPane.showMessageDialog(null, "Ничего не выбрано");
//                return;
//            }
//
//            editButtonClickListener.onClick(jTable1.getSelectedRow());
        });

        deleteButton.setText("Удалить");
        deleteButton.addActionListener(e -> {
           onDelete();

//            if (jTable1.getSelectedRow() == -1) {
//                JOptionPane.showMessageDialog(null, "Ничего не выбрано");
//                return;
//            }
//
//            deleteButtonClickListener.onClick(tableModel.getItem(jTable1.getSelectedRow()));
        });

        saveButton.setText("Сохранить");
        saveButton.addActionListener(evt -> {
            onSave();
//            Map<String, String> map = new HashMap<>();
//            for (JTextField field : fields) {
//                map.put(field.getToolTipText(), field.getText());
//            }
//
//            saveButtonClickListener.onClick(map, tableModel.getItem(currentItem));
        });

        cancelButton.setText("Отмена");
        cancelButton.addActionListener(e ->  {
//            cancelButtonClickListener.onClick(state);

            onCancel();
        });

        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2.setBackground(new java.awt.Color(47, 86, 233));
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(addButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deleteButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(saveButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(100, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(addButton)
                                .addComponent(editButton)
                                .addComponent(deleteButton)
                                .addComponent(cancelButton)
                                .addComponent(saveButton))
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        getContentPane().setBackground(new java.awt.Color(47, 86, 233));
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addGap(8, 8, 8))
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jScrollPane1)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton addButton;
    private JButton deleteButton;
    private JButton editButton;
    private JButton saveButton;
    private JButton cancelButton;

    private JPanel jPanel1;
    private JPanel jPanel2;

    protected JScrollPane jScrollPane1;

    protected JTable jTable1;

    // End of variables declaration//GEN-END:variables

}
