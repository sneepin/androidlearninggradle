/*
 * Created by JFormDesigner on Thu Dec 17 00:43:28 MSK 2015
 */

package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import app.Application;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import controllers.MainController;

/**
 * @author Huan
 */
public class EnterForm extends JFrame {

    private MainController mainController;

    public EnterForm() {
        initComponents();

        mainController = Application.getInstance().getComponent().getMainController();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        button1 = new JButton();
        button2 = new JButton();
        button3 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new FormLayout(
            "28dlu, $lcgap, 160dlu, $lcgap, 31dlu",
            "41dlu, 3*(default, $lgap), 54dlu"));

        //---- button1 ----
        button1.setText("Детали");
        button1.addActionListener(e -> {
            mainController.showAllAssembliesOrFinishedParts();
        });
        contentPane.add(button1, CC.xy(3, 2));

        //---- button2 ----
        button2.setText("Профессии");
        button2.addActionListener(e -> mainController.showProfessions());
        contentPane.add(button2, CC.xy(3, 4));

        //---- button3 ----
        button3.setText("Затраченное время");
        button3.addActionListener(e -> {
            mainController.showSpentTimes();
        });
        contentPane.add(button3, CC.xy(3, 6));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JButton button1;
    private JButton button2;
    private JButton button3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
