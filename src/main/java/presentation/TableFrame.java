package presentation;


import app.MainDataStore;
import table_models.BaseTableModel;
import table_models.Column;
import utils.DatabaseList;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class TableFrame<T> extends BaseTableFrame {

    private BaseTableModel<T> tableModel;
    private State state;
    private AddButtonClickDelegate addButtonClickDelegate;
    private RemoveItemDelegate<T> removeItemDelegate;
    private RemoveLastAddedElementDelegate removeLastAddedElementDelegate;
    private SaveButtonClickListener<T> saveButtonClickListener;
    private JTextField[] fields;
    private JComboBox[] spinners;
    private int currentItem;

    public TableFrame(BaseTableModel<T> tableModel) {
        super();

        this.tableModel = tableModel;
        jTable1.setModel(tableModel);

        showTable();
    }

    public void setAddButtonClickDelegate(AddButtonClickDelegate addButtonClickDelegate) {
        this.addButtonClickDelegate = addButtonClickDelegate;
    }

    public void setRemoveItemDelegate(RemoveItemDelegate<T> removeItemDelegate) {
        this.removeItemDelegate = removeItemDelegate;
    }

    public void setRemoveLastAddedElementDelegate(RemoveLastAddedElementDelegate removeLastAddedElementDelegate) {
        this.removeLastAddedElementDelegate = removeLastAddedElementDelegate;
    }

    public void setSaveButtonClickListener(SaveButtonClickListener<T> saveButtonClickListener) {
        this.saveButtonClickListener = saveButtonClickListener;
    }

    public void showEditPanel(int selectedRow) {
        showEditPanel();

        currentItem = selectedRow;

        JPanel jPanel = new JPanel();
        jPanel.setBackground(new java.awt.Color(174, 234, 255));
        jPanel.setPreferredSize(new Dimension(200, 500));

        fields = new JTextField[tableModel.getColumnCount()];
        spinners = new JComboBox[tableModel.getColumnCount()];

        java.util.List<Column> columnList = tableModel.getColumns();
        for (int i = 0; i < tableModel.getColumnCount(); i++) {
            JLabel jLabel = new JLabel(tableModel.getColumnName(i));
            jPanel.add(jLabel);

            int type = columnList.get(i).getViewType();

            if (type == Column.EDIT_TEXT) {
                Object text = this.tableModel.getValueAt(selectedRow, i);

                JTextField textField = new JTextField(text == null ? " " : text.toString());
                textField.setToolTipText(tableModel.getColumnName(i));
                textField.setPreferredSize(new Dimension(150, 50));
                textField.setSize(100, 100);

                fields[i] = textField;
                jPanel.add(textField);

                continue;
            }

            if (type == Column.SPINNER) {
                java.lang.reflect.Type listType = columnList.get(i).getType();

                List data = null;
                for (DatabaseList list : MainDataStore.mainDataList) {
                    if (list.getType() == listType) {
                        data = list;
                        break;
                    }
                }

                JComboBox box = new JComboBox(data.toArray());
                box.setToolTipText(tableModel.getColumnName(i));

                spinners[i] = box;
                jPanel.add(box);
            }

        }
        jScrollPane1.setViewportView(jPanel);

    }

    @Override
    protected void onAddClick() {
        state = State.ADD;

        showEditPanel(addButtonClickDelegate.add());
    }

    @Override
    protected void onEdit() {
        if (showErorDialog()) {
            return;
        }

        state = State.EDIT;

        showEditPanel(jTable1.getSelectedRow());
    }

    @Override
    protected void onDelete() {
        if (showErorDialog()) {
            return;
        }

        int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to delete this record?");

        System.out.println(dialogResult);

        if(dialogResult == JOptionPane.YES_OPTION) {
            removeItemDelegate.remove(tableModel.getItem(jTable1.getSelectedRow()));

            tableModel.fireTableDataChanged();
        };
    }

    @Override
    protected void onSave() {
        state = State.ADD;

        Map<String, String> map = new HashMap<>();
        for (JTextField field : fields) {
            if (field != null) {
                map.put(field.getToolTipText(), field.getText());
            }
        }

        Map<String, Object> objectMap = new HashMap<>();
        for (JComboBox box : spinners) {
            if (box != null) {
                objectMap.put(box.getToolTipText(), box.getSelectedItem());
            }
        }

        saveButtonClickListener.onClick(map, objectMap, tableModel.getItem(currentItem));

        showTable();

        tableModel.fireTableDataChanged();
    }

    @Override
    protected void onCancel() {
        if (state == State.ADD) {
            removeLastAddedElementDelegate.remove();
        }

        showTable();
    }

    public interface AddButtonClickDelegate {

        int add();
    }

    public interface RemoveItemDelegate<T> {

        void remove(T item);
    }

    public interface SaveButtonClickListener<T> {

        void onClick(Map<String, String> stringFieldValues, Map<String, Object> objectsValues, T t);
    }

    public interface RemoveLastAddedElementDelegate {

        void remove();
    }

    public enum State {
        ADD,
        EDIT;
    }
}
