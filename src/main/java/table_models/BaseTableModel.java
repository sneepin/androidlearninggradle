package table_models;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class BaseTableModel<T> extends AbstractTableModel {

    private List<T> data;

    private List<Column> columns;

    public BaseTableModel(List<T> data, List<Column> columns) {
        this.data = data;
        this.columns = columns;
    }

    public T getItem(int position) {
        return data.get(position);
    }

    public List<Column> getColumns() {
        return columns;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return getValueWithReflection(data.get(rowIndex), columns.get(columnIndex));
    }

    @Override
    public String getColumnName(int column) {
        return columns.get(column).getTitle();
    }

    private String getValueWithReflection(T t, Column column) {
        try {
            Field field = t.getClass().getDeclaredField(column.getFieldName());
            field.setAccessible(true);

            return field.get(t).toString();
        } catch (NoSuchFieldException | IllegalAccessException e) {

            Field[] fields = t.getClass().getDeclaredFields();
            for (Field f : fields) {
                try {
                    f.setAccessible(true);

                    Object object = f.get(t);

                    Field resultField = object.getClass().getDeclaredField(column.getFieldName());
                    resultField.setAccessible(true);

                    return resultField.get(object).toString();
                } catch (NoSuchFieldException | IllegalAccessException e1) {

                }
            }
        }

        return "";
    }
}
