package table_models;


import java.lang.reflect.Type;

public class Column {

    public static final int EDIT_TEXT = 0;

    public static final int SPINNER = 1;

    public static final int CHECK_BOX = 2;

    private String title;

    private String fieldName;

    private int viewType;

    private Type type;

    public Column(String title, String fieldName, int viewType) {
        this.title = title;
        this.fieldName = fieldName;
        this.viewType = viewType;
    }

    public Column(String title, String fieldName, int viewType, Type type) {
        this.title = title;
        this.fieldName = fieldName;
        this.viewType = viewType;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
