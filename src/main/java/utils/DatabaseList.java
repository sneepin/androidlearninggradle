package utils;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class DatabaseList<T> extends ArrayList<T> {

    private Type type;

    public DatabaseList(Type type) {
        super();

        this.type = type;
    }

    public Type getType() {
        return type;
    };
}
