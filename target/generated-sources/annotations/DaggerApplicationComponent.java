import app.ApplicationComponent;
import dagger.internal.ScopedProvider;
import app.ConnectionCreater;
import data.DataModule_ProvideAssemblyOrFinishedPartDaoFactory;
import data.DataModule_ProvideConnectionCreaterFactory;
import dao.AssemblyOrFinishedPartDao;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DaggerApplicationComponent implements ApplicationComponent {
  private Provider<ConnectionCreater> provideConnectionCreaterProvider;
  private Provider<AssemblyOrFinishedPartDao> provideAssemblyOrFinishedPartDaoProvider;

  private DaggerApplicationComponent(Builder builder) {  
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {  
    return new Builder();
  }

  public static ApplicationComponent create() {  
    return builder().build();
  }

  private void initialize(final Builder builder) {  
    this.provideConnectionCreaterProvider = ScopedProvider.create(DataModule_ProvideConnectionCreaterFactory.create(builder.dataModule));
    this.provideAssemblyOrFinishedPartDaoProvider = ScopedProvider.create(DataModule_ProvideAssemblyOrFinishedPartDaoFactory.create(builder.dataModule, provideConnectionCreaterProvider));
  }

  @Override
  public AssemblyOrFinishedPartDao getDao() {  
    return provideAssemblyOrFinishedPartDaoProvider.get();
  }

  public static final class Builder {
    private DataModule dataModule;
  
    private Builder() {  
    }
  
    public ApplicationComponent build() {  
      if (dataModule == null) {
        this.dataModule = new DataModule();
      }
      return new DaggerApplicationComponent(this);
    }
  
    public Builder dataModule(DataModule dataModule) {  
      if (dataModule == null) {
        throw new NullPointerException("dataModule");
      }
      this.dataModule = dataModule;
      return this;
    }
  }
}

