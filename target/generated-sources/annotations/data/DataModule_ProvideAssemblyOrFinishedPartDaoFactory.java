package data;

import app.ConnectionCreater;
import dagger.internal.Factory;
import dao.AssemblyOrFinishedPartDao;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DataModule_ProvideAssemblyOrFinishedPartDaoFactory implements Factory<AssemblyOrFinishedPartDao> {
  private final DataModule module;
  private final Provider<ConnectionCreater> connectionCreaterProvider;

  public DataModule_ProvideAssemblyOrFinishedPartDaoFactory(DataModule module, Provider<ConnectionCreater> connectionCreaterProvider) {
    assert module != null;
    this.module = module;
    assert connectionCreaterProvider != null;
    this.connectionCreaterProvider = connectionCreaterProvider;
  }

  @Override
  public AssemblyOrFinishedPartDao get() {  
    AssemblyOrFinishedPartDao provided = module.provideAssemblyOrFinishedPartDao(connectionCreaterProvider.get());
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<AssemblyOrFinishedPartDao> create(DataModule module, Provider<ConnectionCreater> connectionCreaterProvider) {
    return new DataModule_ProvideAssemblyOrFinishedPartDaoFactory(module, connectionCreaterProvider);
  }
}

