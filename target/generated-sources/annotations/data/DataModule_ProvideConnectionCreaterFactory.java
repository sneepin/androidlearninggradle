package data;

import app.ConnectionCreater;
import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated("dagger.internal.codegen.ComponentProcessor")
public final class DataModule_ProvideConnectionCreaterFactory implements Factory<ConnectionCreater> {
  private final DataModule module;

  public DataModule_ProvideConnectionCreaterFactory(DataModule module) {  
    assert module != null;
    this.module = module;
  }

  @Override
  public ConnectionCreater get() {  
    ConnectionCreater provided = module.provideConnectionCreater();
    if (provided == null) {
      throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
    return provided;
  }

  public static Factory<ConnectionCreater> create(DataModule module) {
    return new DataModule_ProvideConnectionCreaterFactory(module);
  }
}

